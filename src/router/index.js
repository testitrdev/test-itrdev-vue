import {
  createRouter,
  createWebHistory,
} from 'vue-router';
import store from '../store';
import Home from '../views/Home.vue';

const ifNotAuthenticated = (to, from, next) => {
  if (!store.getters.isAuthenticated) {
    next();
    return;
  }
  next('/');
};

const ifAuthenticated = (to, from, next) => {
  if (store.getters.isAuthenticated) {
    next();
    return;
  }
  next('/login');
};

const routes = [{
  path: '/',
  name: 'Home',
  component: Home,
  beforeEnter: ifAuthenticated,
},
{
  path: '/about',
  name: 'About',
  component: () => import(/* webpackChunkName: "about" */ '../views/About.vue'),
  beforeEnter: ifAuthenticated,
},
{
  path: '/login',
  name: 'Login',
  component: () => import(/* webpackChunkName: "login" */ '../views/Login.vue'),
  beforeEnter: ifNotAuthenticated,
},
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
