import axios from 'axios';
import { routerKey } from 'vue-router';
// import apiCall from '../../utils/api';

const state = () => ({
  token: localStorage.getItem('user-token') || '',
  status: '',
  hasLoadedOnce: false,
  user: '',
});

const getters = {
  isAuthenticated: (state) => !!state.token,
  authStatus: (state) => state.status,
  getUser: (state) => state.user,
};

const actions = {
  auth_request: ({
    commit,
  },user) => new Promise((resolve, reject) => {
    commit('auth_request');
    axios({
      method: 'POST',
      data: user,
      url: 'https://api.kotofey.ninja/auth/login',
    })
      .then((resp) => {
        if(resp.data.status == 'success'){
          localStorage.setItem('user-token', resp.data.token);
          localStorage.setItem('user', resp.data.user);
          axios.defaults.headers.common['Authorization'] = resp.data.token
          commit('auth_success', resp.data);
          resolve(resp.data);
        } else {
          commit('auth_error',resp.data);
          reject(err)
        }
      })
      .catch((err) => {
        commit('auth_error', err);
        localStorage.removeItem('user-token');
        reject(err);
      });
  }),
  // auth_logout: ({
  //   commit,
  // }) => new Promise((resolve) => {
  //   commit(auth_logout);
  //   localStorage.removeItem('user-token');
  //   resolve();
  // }),
};

const mutations = {
  auth_request: (state) => {
    state.status = 'loading';
  },
  auth_success: (state, resp) => {
    state.status = 'success';
    state.token = resp.token;
    state.user = resp.user;
    state.hasLoadedOnce = true;
  },
  auth_error: (state) => {
    state.status = 'error';
    state.hasLoadedOnce = true;
  },
  auth_logout: (state) => {
    state.token = '';
    state.user = '';
  },
};

export default {
  state,
  getters,
  actions,
  mutations,
};
